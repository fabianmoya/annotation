/* This file uses es5 syntax since 'babel-node not meant for production use' */

var express = require('express');
var path = require('path');
var compression = require('compression');

var port = 3000;
var app = express();

app.use(compression());
app.use(express.static('build'));

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log('App running on port: http://localhost:'+port);
    }
});
