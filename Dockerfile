FROM node:6.12.2-alpine

MAINTAINER Bruce MacFarlane <bmacfarlane@cyngn.com>

ENV HOME=/home/app

# Create /mycode directory
RUN mkdir -p $HOME/mycode

# Install mycode dependencies
COPY package.json $HOME/mycode/

# USER app
WORKDIR $HOME/mycode

RUN cd $HOME/mycode/ && npm cache clean && yarn install

COPY . $HOME/mycode

RUN cd $HOME/mycode/ && yarn build

EXPOSE 3000

# the CMD below if overriden by the docker-compose 'command' when in development
CMD [ "node", "server/distServer.js" ]
