# cyngn-annotation-app
Application for creating and reviewing annotations.

<small>*(Staging URL: http://anno-staging.cyngn.com)*</small>

## Dev Setup
- Install [NVM](https://github.com/creationix/nvm/blob/master/README.md)
- Install Node v6 LTS (currently v6.11.3):
    ```
    $ nvm install v6.11.3
    ```
- Globally install [Yarn](https://yarnpkg.com/en/docs/install).
- Run yarn install:
  ```
  $ yarn install
  ```
- Run in DEV mode:
  ```
  $ yarn start
  ```
- Run local development API (__*run this command in a seperate terminal window__, *and make sure to set USE_LOCAL_API in /src/appConfig/index.js to true*):
  ```
  $ yarn devapi
  ```
- Run tests:
  ```
  $ yarn test
  ```
- Build distribution output (generally don't need to do this since our build process will deploy to staging env):
  ```
  $ yarn build
  ```

- If you want to test the Docker image locally (* requires [Docker](https://docs.docker.com/docker-for-mac/install/) installed locally):
    ```
    $ docker build -t mytestimage .
    $ docker run -d -p 3000:3000 mytestimage:latest
    ```

## Development Workflow
*All work must start as a Jira ticket and a new branch off of 'master'*
1. Create new git branch off of 'master' based on Jira ticket (__Never push directly to 'master' branch__)
    ```
    $ git checkout master
    $ git pull
    $ git checkout -b ${YOUR_CYNGN_EMAIL_ID}/${DESCRIPTION}/${JIRA_ID}
    ```
2. Work on new feature/task/bug in local branch, include unit tests of business logic.
3. When ready, run tests locally to make sure all is working.
4. If all tests have passed, merge in an updated master branch to your new branch (to avoid conflicts), then push new branch to remote:
    ```
    $ git push -u origin ${YOUR_CYNGN_EMAIL_ID}/${DESCRIPTION}/${JIRA_ID}
    ```
5. Go into Github and file a pull request
6. Pull request triggers CircleCI build and test which will get reported back to the pull request in Github.
6. Code review will happen off of the pull request within Github (we'll probably move the code review process to a better tool down the line).
7. When pull request is ready, branch is merged and committed to master branch (at some point we'll automate this process as well).
8. Remove work branch off of the origin remote.

## Build And Deploy Process To Staging Environment
**Pull Requests:**
1. Pull request triggers CircleCi process.
2. Process will install and run unit tests.
3. CircleCi will report back to the pull request if checks have passed.

**Master branch merges and commits:**
1. Merging or committing directly to the master branch triggers an AWS CodePipeline process (* __*do not commit code directly to master branch*__).
2. AWS CodePipeline downloads project from github master branch.
3. AWS CodeBuild process is kicked off which (see buildspec.yml):
    - generates a Dockerrun.aws.json file (which will be used during the deploy stage).
    - Docker build is run (from the Dockerfile), tagged and uploaded to AWS ECR (Elastic Container Registry)
4. AWS CodeDeploy process is kicked off from the generated Dockerrun.aws.json file which is pushed to our Elastic Beanstalk Multi-Container Docker Instance.


...
