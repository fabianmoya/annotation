module.exports = [
  {
    "image_height": 1080,
    "image_id": 1,
    "image_label": "car",
    "image_url": "https://s3-us-west-1.amazonaws.com/cyngn-annotation-app-processing/08047.jpg",
    "image_width": 1920,
    "job_type": "annotate",
    "previous_annotations": [
      {
        "annotation_id": 1,
        "x": 220,
        "y": 280,
        "width": 250,
        "height": 120,
        "editable": false
      },
      {
        "annotation_id": 2,
        "x": 1270,
        "y": 390,
        "width": 40,
        "height": 160
      }
    ]
  },
  {
    "annotation": {
      "annotation_id": 1,
      "x": 820,
      "y": 60,
      "width": 60,
      "height": 310
    },
    "error_obj": [
      {
        "error_id": 1,
        "error_name": "Wrong tag"
      },
      {
        "error_id": 2,
        "error_name": "tag(s) size around object is too large"
      },
      {
        "error_id": 3,
        "error_name": "tag(s) doesn’t cover entire object"
      },
      {
        "error_id": 4,
        "error_name": "Missing tag(s)"
      },
      {
        "error_id": 5,
        "error_name": "Incorrect occlusion"
      },
      {
        "error_id": 6,
        "error_name": "Other"
      }
    ],
    "img_obj": {
      "image_height": 1080,
      "image_id": 1,
      "image_label": "car",
      "image_url": "https://s3-us-west-1.amazonaws.com/cyngn-annotation-app-processing/08047.jpg",
      "image_width": 1920
    },
    "job_type": "verify"
  },
  {
    "annotation": [
      {
        "annotation_id": 1,
        "coordinates": {
          "x": 1200,
          "y": 0,
          "width": 640,
          "height": 600
        },
        "occluded": false
      },
      {
        "annotation_id": 2,
        "coordinates": {
          "x": 530,
          "y": 220,
          "width": 260,
          "height": 180,
          "editable": false
        },
        "occluded": false
      }
    ],
    "error_obj": [
      {
        "error_id": 1,
        "error_name": "Wrong tag"
      },
      {
        "error_id": 2,
        "error_name": "tag(s) size around object is too large"
      },
      {
        "error_id": 3,
        "error_name": "tag(s) doesn’t cover entire object"
      },
      {
        "error_id": 4,
        "error_name": "Missing tag(s)"
      },
      {
        "error_id": 5,
        "error_name": "Incorrect occlusion"
      },
      {
        "error_id": 6,
        "error_name": "Other"
      }
    ],
    "img_obj": {
      "image_height": 1080,
      "image_id": 1,
      "image_label": "car",
      "image_url": "https://s3-us-west-1.amazonaws.com/cyngn-annotation-app-processing/08047.jpg",
      "image_width": 1920
    },
    "job_type": "confirm"
  }
]
