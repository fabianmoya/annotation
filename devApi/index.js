const express = require('express');
const cors = require('cors');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cors());

let tasks = require('./mock-data/tasks')

// ROUTES
const LOGIN_API_PATH = '/api/login';
const TASK_API_PATH = '/api/task';

const MAX_DELAY = 1400;
const getDelay = () => Math.floor(Math.random() * MAX_DELAY) + 100;

app.post(LOGIN_API_PATH, (req, res) => {
  const currentDate = new Date();
  const successPayload = {
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF90b2tlbiI6ImV5SnJhV1FpT2lKdFdtTkdlRW8wWWtwdVNrMXJZblptWkV0bU1YUkdTV0pvU2xwVFhDOVZjVU5WYlc1a1NXTllURzlDYnowaUxDSmhiR2NpT2lKU1V6STFOaUo5LmV5SnpkV0lpT2lJMk1qVXhOREl4TXkweFpXVmhMVFF5T0RndE9URXdOUzB6WW1FeE5EWXdNMkU1TXpNaUxDSmxiV0ZwYkY5MlpYSnBabWxsWkNJNmRISjFaU3dpYVhOeklqb2lhSFIwY0hNNlhDOWNMMk52WjI1cGRHOHRhV1J3TG5WekxXVmhjM1F0TVM1aGJXRjZiMjVoZDNNdVkyOXRYQzkxY3kxbFlYTjBMVEZmYXpOTk5uZHdiREJ4SWl3aWNHaHZibVZmYm5WdFltVnlYM1psY21sbWFXVmtJanBtWVd4elpTd2lZMjluYm1sMGJ6cDFjMlZ5Ym1GdFpTSTZJbkJ5WVhOb1lXNTBJaXdpWVhWa0lqb2lNM0Z2Ykd3eGNYUTBhM0J5YlRZeWFXUnJZbVl4ZG1NMVoyZ2lMQ0psZG1WdWRGOXBaQ0k2SW1NeU16UXlOems0TFRBd04yUXRNVEZsT0MxaFl6bG1MVFE1T0dNNE1tSTFNREJpWkNJc0luUnZhMlZ1WDNWelpTSTZJbWxrSWl3aVlYVjBhRjkwYVcxbElqb3hOVEUyTnpRd01EYzJMQ0p3YUc5dVpWOXVkVzFpWlhJaU9pSXJPVEU1T0RRMU1EazRORFV3SWl3aVpYaHdJam94TlRFMk56UXpOamMyTENKcFlYUWlPakUxTVRZM05EQXdOellzSW1WdFlXbHNJam9pY0hKaGMyaGhiblJBWTNsdVoyNHVZMjl0SW4wLk1jQzNNSHRRaDc1QlR3cGtKOVlsMnRDU2ZrVTdpMkZvc1VMc0xtM0ZiNGF5VlQ2U2lKM3VVY1lqWUo1U2h5TTlZWTFyR19kSl80b0lZVnh2ME4ydUxHVzZJNnhyTXlacWRFb0YwNi1oeV9GT2FuclZRWmNfbmxqNjAtUUNXdWlsNE5xclVOQzhIbUwyZ2xIUnl2cjNGSVYwTXJqUEtZbXNMeFRvMVM4Z08tVDR6RFVkWER1bWx6dzVQVkJ4bUhYNnB1ZS03Z3BURnpJeHZaWkhBcXd0R3o3ZWVBX3ZYUGVEdGdNRS1hUmw4Y1RnNm5EUGc2YlVNT2FDcXcxQzhTVURiNERWN0NLSEh0LTFsWURxb0xVTVlPX3JiT1l5WklPS25GNmNUdm1iVnpjSDd6c2pUYjZGeXl2c1FERnRWRGtsTnE3c3dtY2RwejlVbjJaVUp1SzZ5USIsInJlZnJlc2hfdG9rZW4iOiJleUpqZEhraU9pSktWMVFpTENKbGJtTWlPaUpCTWpVMlIwTk5JaXdpWVd4bklqb2lVbE5CTFU5QlJWQWlmUS5pcDM3Z3VDS2JITVJoVkZWdllKcngzT2czVWFtLUJPUmV5RFE0dHMzWElwVURESnVlQ3hFT0xTNXNXYUljdVZXZ0VGYno3MUVEMS12R0Rodnp6Q2Z6d2pYLWdMMUVtYTh0TmVpcXVGRktUY3FmdDdrV0RyeGtkWl9LNHNPNXY3N0E4WFQzczRKQmVkM21rZ2RBa1BuTVFCS0hhSVcyVjNlWUQ5M2R1TXAwQllQTG9CYWpES1BFOEFVaEhieUh5VkxuQlRhMkgtbkx3cXA5bFRBUUlRLUNvS29EbXdGcUlGUFM1TEZmZjVZT19xeDljRkticU9nTnY0YmVGUnRjQWdubGIyc3NrbVVXaHNGV19raXVBb2MxVEVVdXI1bm1fa09zZWEwdU5xUUVDTXR5czJsM2l3ZWJyQ280ZmVmWHBWNjZMRVJEU21RRmJ6cmx3WlFfaWhxbFEuWmNzQW02dk1SX2NxelIwYy5vQXlSRUw3VWhFVkZnQkUzWXNsNnJFQ3JBOUJlRS14YW5ZWUVHaDg1U0x0aHY4YXc3Zll5dkpIbWRkeGlFVkdvaEhMUkxHZ1FzZlhKbnFCaS1UZzZJcUJSYldYLXdIdmZ3Q1B3Z2t5OXVwMVZtRmk3R2c2SW9lcmVyeEhFYk1NQk5oZFVINjhELUVKV0w1a0FvMi1kM2gtTHc0V3Q5UGt3b1ZOTVZLY24zbFNwWHNoRW9SLUNieHdEMmUzYzczRU13VTRXdWJjaHp0a3hDTUFacmY2cTNOQ2MtUy1oQW12MkJuZm1qQ25XQnlWOTNOTldISEMxRW5TQTRJc0tPUkJVZzRDVGFveUduUEFMN25FQkhKSU9lSjFoV183Y0RZbWdNWlkxcGVScTF6Szg1clh6MGIzNG4zVjFRUTNIb3ZrN0lUdThEQllBaHllQ2ZyR0RLajBBOG5TeFJmeTJiaTBLZTk1SGhmZzgzYkVnN0VIN2RNaEFlMTFrUFcxSGNBMjNmRkVPTXhhRVJjRFBab3VQYWxiVF9pZThBa1dhdGdVQlk4TEVGNHlWaUdBaGYxZXU2MjFaRW1OR0xCT2twQ3B6UC1xWVAyRUhKdy1jMmZMZXh0VUVMbGtDRnpzUEttNDJWaGJ2azRub3JvYTVwZlg2Q3ZTSlFoenk3ZDNFZnZwQkFZdFotMHVteVdsaUJVM1BSaU9yUGdWU0kyMVFOcW02cUtBUzJMalAxeFRNQXBZODRKMnNqbjduZ2h5a25qVWxXanZfa1ExN2E5M2JaUE95RlpmV2dOZElqekxnaVhBSkdTeGdLcGtXaWpFblNJcUV4dExqNWVZdDc2TjZOYy1sSFlneHFVOWtWczVCVGxycm5QRkNSMUJCMGdrZVc3VFlrTXdZd2xNU2dVVk5tQW5XR25uQ3NlX19rRHpHMko3WE02TXplSlA0SDlUNG12ZTVVeTVBR0VhNzhRbXp1NWdVa3ltNklMOC1qT0NkYXRWTGR2bXZTUmRKRjJwMEc0TnZuMXdJSGo4X0tONTJXVUhRbWJISlpOZkFwejhveVZHaDRjU1FKYktvYlFkbEIyZHJhbHZnODBaUkNZNmEydENRWklEQTlickNKZkJSdm9TRmNFRmpJOVl1RUl5aGRzUUVRZk9nM3NqMnlJd2NRWGFPX21Td1A4dmtwM0RWdllvWUVXQzdlT0hOeHloSlZfQ2lnaDl6RFlkRVZ0M1FSSTBBU2dRV243Ti1qRzlUNzJyQ2lsb0ZjOXdWQnNNUmVNVlhFSUdZS2pOMVZKZ3h2UUhSODI4WHY4YUZGNWhmRTNPMmxhQWswQjRHUTlaYWwxQUxOWGxiVlBrT3NNek5VbmJQczBUT210UVVLOGYyaVg3VHRjdjlmcEVHcmZRemZmcWgwbGZKNm9kdXdBTEE1cUlxMVRnRVVjQlRxbGlYWlQ3YW5uTWZvQ2M0aHNXWlpSY2dHVTU5VFduVi1LWG5UVHY2cXJacUZUelgzdnZ5OXlJTW9ZTGdFNHpsenNsOXJfVm9rRnNzaUlXVkRBMktVdVF2Vk9KVUlWbk5TVUdrY3Q3dVpDOUl4SGE0YWJmSHUwMVRBUTRaTkdfSU5jZGNoZnFLRXhkeEM0MFZ5NVFkRmlfLUVSNXJYYTdaQXRUX2NlQU4uTmEyWnZKV2xIMjMyTURLdHJjRU5DQSIsImFjY2Vzc190b2tlbiI6ImV5SnJhV1FpT2lKbFNITkRZMGhRU2pWSE1qRnpibHd2YURsSmRrbFpTalpjTDF3dlZXUmpURmxXYVdkMWRVRklRemxYWTAwMFBTSXNJbUZzWnlJNklsSlRNalUySW4wLmV5SnpkV0lpT2lJMk1qVXhOREl4TXkweFpXVmhMVFF5T0RndE9URXdOUzB6WW1FeE5EWXdNMkU1TXpNaUxDSmxkbVZ1ZEY5cFpDSTZJbU15TXpReU56azRMVEF3TjJRdE1URmxPQzFoWXpsbUxUUTVPR000TW1JMU1EQmlaQ0lzSW5SdmEyVnVYM1Z6WlNJNkltRmpZMlZ6Y3lJc0luTmpiM0JsSWpvaVlYZHpMbU52WjI1cGRHOHVjMmxuYm1sdUxuVnpaWEl1WVdSdGFXNGlMQ0pwYzNNaU9pSm9kSFJ3Y3pwY0wxd3ZZMjluYm1sMGJ5MXBaSEF1ZFhNdFpXRnpkQzB4TG1GdFlYcHZibUYzY3k1amIyMWNMM1Z6TFdWaGMzUXRNVjlyTTAwMmQzQnNNSEVpTENKbGVIQWlPakUxTVRZM05ETTJOellzSW1saGRDSTZNVFV4TmpjME1EQTNOaXdpYW5ScElqb2lZVGRsWkRVMVl6Y3RaVFUzWkMwME1UTXhMVGd6TmpRdE1XWXpNVGc0TW1RMk5qQTVJaXdpWTJ4cFpXNTBYMmxrSWpvaU0zRnZiR3d4Y1hRMGEzQnliVFl5YVdSclltWXhkbU0xWjJnaUxDSjFjMlZ5Ym1GdFpTSTZJbkJ5WVhOb1lXNTBJbjAuT1JBMVNFZ3pmNHBwQzM2YTVpWFNoYWMwbXd4YkMySjJMcFVsbkZxWTVhS0RxT05EdDBpdGhoWUVzVFFCLUwtSHR6bEVEMUtGd28wNHR3b2JmaTlCVzh2TG52eDZyemM0MHlLUTlrTnRwek13ZmF0SzdKbTdMNEM1OW8tN2k1aldacldVQWpvQ0V4TnVzZEwzOVNPcHlVeHAzTU1wU2d5YVhSY1J3V09Jd3Z5aTdScndtVHVaMXNpN2NEOUx3RDlNblRibzZ4V3V0Q0ZkYm5DaFc4VTlMTG9UOWRfT0trWFNqbnYwanFEeTg0cXNaZFJidkVDUjVwaWNvcjBndEFGV0V6U2U0b1BFLUROeFFmWFBXN1BOS0xXaVJLLXZqZ0l5VWtfd1hrSHVtMG53WkhaT1BXOWdQUjVaWXh3blJ2MGVzRjFGVE4zeGg0QXJTWEhGbnJ1c3NBIn0.CHD0qBqw7TrUz96bI2AnVJMAgjBQMrT72qMyYt0vjMo",
    "type": "success",
    "expiration": currentDate.setDate(currentDate.getDate() + 1)
  };

  const errorPayload = {
    "message": "username or password is incorrect",
    "type": "error"
  };

  let payload;
  const username = req.body.username;
  const password = req.body.password;
  if(username && password) {
    payload = successPayload
  }
  else {
    payload = errorPayload;
  }

  if (true) { // set to false to test error
    setTimeout(() => { res.json(payload); }, getDelay());
  }
  else {
    res.status(404).json({ error: 'There was an error.' });
  }
});

app.get(TASK_API_PATH, (req, res) => { 
  const randomPosition = Math.floor(Math.random() * Math.floor(3))
  let payload = tasks[randomPosition]

  if (req.query.type) {
    switch(req.query.type) {
      case 'annotate':
        payload = tasks[0]
      break;
      case 'verify':
        payload = tasks[1]
      break;
      case 'confirm':
        payload = tasks[2]
      break;
    }
  }

  res
    .status(200)
    .json(payload)
})

app.post(TASK_API_PATH, (req, res) => { 
  const payload = {
    message: 'Successful'
  }

  res
    .status(200)
    .json(payload)
})

console.log(`Starting server on port 3001`);
app.listen(3001);
