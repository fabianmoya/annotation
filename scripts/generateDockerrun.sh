#!/bin/bash

FILE="Dockerrun.aws.json"
TAG="$1"
if [ -z "$TAG" ]; then
   TAG="latest"
fi

cat > $FILE <<- EOM
{
  "AWSEBDockerrunVersion": 2,
  "volumes": [],
  "containerDefinitions": [
    {
      "name": "cyngn",
      "image": "320959454024.dkr.ecr.us-east-1.amazonaws.com/cyngn-annotation-app:$TAG",
      "environment": [],
      "essential": true,
      "memory": 512,
      "mountPoints": []
    },
    {
      "name": "nginx",
      "image": "320959454024.dkr.ecr.us-east-1.amazonaws.com/cyngn-web-nginx:latest",
      "essential": true,
      "memory": 128,
      "portMappings": [
        {
          "hostPort": 80,
          "containerPort": 80
        }
      ],
      "links": [
        "cyngn"
      ],
      "mountPoints": []
    }
  ]
}
EOM
