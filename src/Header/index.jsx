//@flow
import React from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {logoutUser} from '../actions/userActions';
import logo from '../common/assets/logo.svg';
import './styles/index.scss';

type Props = {
    onLogoutSubmit: Function
};

const Header = (props : Props) => {
    let logout = null;
    let topMenu = null;
    let navButtons = null;

    if (props.isLoggedIn) {

        logout = <div onClick={props.onLogoutSubmit} className="header__menu-button">Log Out</div>
        navButtons = (
            <div className="header__items nav">
                <div className="nav__items">
                    <NavLink exact to="/addUser" activeClassName="selected">
                        <div className="header__menu-button">Add User</div>
                    </NavLink>
                </div>
                <div className="nav__items">
                    <NavLink exact to="/tags" activeClassName="selected">
                        <div className="header__menu-button">Tags</div>
                    </NavLink>
                </div>
                <div className="nav__items">
                    <NavLink exact to="/job" activeClassName="selected">
                        <div className="header__menu-button">Jobs</div>
                    </NavLink>
                    {/* <div className="nav__subitems" >
                        <NavLink exact to="/job" activeClassName="selected">
                            <div className="header__menu-button">Jobs</div>
                        </NavLink>
                        <NavLink exact to="/editJob" activeClassName="selected">
                            <div className="header__menu-button">Edit Job</div>
                        </NavLink>
                    </div> */}
                </div>

                {logout}
            </div>
        );

    }else{
        navButtons = (
            <div className="header__items nav">
                <div className="nav__items">
                    <NavLink exact to="/login" activeClassName="selected">
                        <div className="header__menu-button">Login</div>
                    </NavLink>
                </div>
                <div className="nav__items">
                    <NavLink exact to="/registration" activeClassName="selected">
                        <div className="header__menu-button">Registration</div>
                    </NavLink>
                </div>
                {logout}
            </div>
        );
    }
    topMenu = (
        <div className="header">
            <div className="header__wrapper">
                <div className="header__label">
                    <span className="header__text">ADMIN</span>
                </div>

                {navButtons}

                <img className="logo" src={logo} alt="CYNGN LOGO"/>
            </div>

        </div>
    );
    return (topMenu);
};

const mapStateToProps = state => ({isFetching: state.user.isFetching, isLoggedIn: state.user.isLoggedIn, errorMessage: state.user.errorMessage});

const mapDispatchToProps = (dispatch) => ({
    onLogoutSubmit() {
        dispatch(logoutUser());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
