export type Point = {
    x: number,
    y: number
};
export type Size = {
    width: number,
    height: number
};
export type Rect = {
    x: number,
    y: number,
    width: number,
    height: number
}
export type ScrollPercentage = {
    top: number,
    left: number
};
export type AnnotationConfig = {
    id: string,
    top: number,
    left: number,
    width: number,
    height: number
};
