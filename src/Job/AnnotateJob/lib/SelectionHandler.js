//@flow
import type {Point} from './types';

class SelectionHandler {
  el: HTMLElement;
  touchActive: boolean = false;
  startPos: Point;
  endPos: Point;
  offsetPos: Point;


  constructor(el: HTMLElement) {
    this.el = el;
    this.setup();
  }

  setup() {
    // click and drag event listeners
    this.el.addEventListener("mousedown", this.handleTouchStart, false);
    this.el.addEventListener("mousemove", this.handleTouchMove, false);
    this.el.addEventListener("mouseup", this.handleTouchEnd, false);
  }

  handleTouchStart = (e: any) => {
    this.touchActive = true;
    //$FlowFixMe
    const sl = this.el.parentNode ? this.el.parentNode.scrollLeft : 0;
    //$FlowFixMe
    const st = this.el.parentNode ? this.el.parentNode.scrollTop : 0;
    this.offsetPos = { x: this.el.offsetLeft - sl, y: this.el.offsetTop - st };
    this.startPos = {x: e.pageX - this.offsetPos.x, y: e.pageY - this.offsetPos.y};

  }

  handleTouchMove = (e: any) => {
    if(this.touchActive) {
      this.endPos = {x: e.pageX - this.offsetPos.x, y: e.pageY - this.offsetPos.y};
    //    console.log('endpos',this.endPos)
    //   console.log(e.clientX)
      this.handleUpdatedSelection();
    }
  }

  handleTouchEnd = (e: any) => {
    this.handleUpdatedSelection(true);
    this.touchActive = false;
    this.startPos = null;
    this.endPos = null;
  }

  handleUpdatedSelection = (selComplete: boolean = false) => {
    const s = this.startPos;
    const e = this.endPos;
    //console.log('start',s)
    //console.log('ed',s)
    if(s && e) {
      const x = Math.min(s.x, e.x);
      const y = Math.min(s.y, e.y);
      const w = Math.abs(e.x - s.x);
      const h = Math.abs(e.y - s.y);
      const rect = {x,y,w,h};
      var event = new CustomEvent('selection', { detail: { rect: rect, selectionComplete: selComplete } });
      this.el.dispatchEvent(event);
    }
  }

  addEventListener = (name: string, callback: Function, bubbles: boolean) => {
    this.el.addEventListener(name, callback, bubbles);
  }

  removeEventListener = (name: string, callback: Function) => {
    this.el.removeEventListener(name, callback);
  }
}

export default SelectionHandler;
