//@flow
import type { Rect, Point }
from './types';

/*
{ top:56, left:92, width: 192, height: 140 }
*/

class Annotation {
    parent : HTMLDivElement; // container
    // config: AnnotationConfig; // conf
    el : HTMLDivElement;
    conversionScale : number = 1;
    revertScale : number = 1;
    multiplier : number = 1;
    editable : boolean = false;
    currentRect : Rect;
    touchActive : boolean = false;
    initialDrawComplete : boolean = true;
    startPos : Point;
    delta : Point;
    selected = null; // Object of the element to be moved
    x_pos = 0;
    y_pos = 0; // Stores x & y coordinates of the mouse pointer
    x_elem = 0;
    y_elem = 0; // Stores top, left values (edge) of the element

    constructor( parentEl : HTMLDivElement, color) {
        this.parent = parentEl;
        // console.log( this.parent );
        this.createElement(color);
    }

    createElement(color) {
        this.el = document.createElement( "div" );
        this.el.classList.add( "annotation" );

        this.el.style.position = "absolute";
        // this.el.style.backgroundColor = color;
        // this.el.style.opacity = "0.4";
        // this.el.style.border = 'solid 1px ' + color;
        this.parent.appendChild( this.el );


        this.resizer = document.createElement('div');
        this.resizer.className = 'resizer';
        this.resizer.style.width = '10px';
        this.resizer.style.height = '10px';
        this.resizer.style.background = 'red';
        this.resizer.style.position = 'absolute';
        this.resizer.style.right = 0;
        this.resizer.style.bottom = 0;
        this.resizer.style.cursor = 'se-resize';
        this.resizer.style.display = 'block';

        this.el.appendChild(this.resizer);

        this.el.addEventListener( "mousedown", this._drag_init, false );
        this.el.addEventListener( "mousemove", this._move_elem, false );
        this.el.addEventListener( "mouseup", this._destroy, false);

        this.resizer.addEventListener('mousedown', this._initResize, false);
    }

    setSelectionRect( rect : Rect, initialDrawComplete : boolean = false ) {
        this.currentRect = rect;
        this.initialDrawComplete = initialDrawComplete;
        if ( initialDrawComplete ) {

        }
    }

    updateDisplay( conversionScale : number, multiplier : number = 1 ) {
        this.conversionScale = conversionScale;
        this.multiplier = multiplier;
        const c = this.currentRect;

        if ( c ) {
            this.el.style.width = Math.ceil( c.width * conversionScale * multiplier ) + "px";
            this.el.style.height = Math.ceil( c.height * conversionScale * multiplier ) + "px";
            this.el.style.top = Math.ceil( c.y * conversionScale * multiplier ) + "px";
            this.el.style.left = Math.ceil( c.x * conversionScale * multiplier ) + "px";
        }
    }

    removeActions() {
        this.el.removeEventListener( "mousedown", this._drag_init, false );
        this.el.removeEventListener( "mousemove", this._move_elem, false );
        this.el.removeEventListener( "mouseup", this._destroy, false);

        this.el.classList.add("non-editable");

        this.el.removeChild(this.el.getElementsByClassName('resizer')[0]);

        this.resizer.removeEventListener('mousedown', this._initResize, false);
    }

    _destroy = ( e ) => {

        if ( this.initialDrawComplete ) {
            e.stopPropagation( );
            this.startPos = null;
            this.endPos = null;
            this.touchActive = false;
            this.selected = null;

            this.x_elem = this.x_pos - this.el.offsetLeft;
            this.y_elem = this.y_pos - this.el.offsetTop;

            var left = this.x_pos - this.x_elem;
            var top = this.y_pos - this.y_elem;
            this.el.style.left = left + 'px';
            this.el.style.top = top + 'px';

            const newR = {
                x: this.currentRect.x,
                y:this.currentRect.y,
                width: (this.currentRect.width ) ,
                height:(this.currentRect.height )
            };

            this.setSelectionRect( newR, true )
        }

    }

    _move_elem = ( e ) => {
        e.stopPropagation( );
        if ( this.touchActive ) {
            console.log( '1' )
        }
        this.x_pos = document.all
            ? window.event.clientX
            : e.pageX;
        this.y_pos = document.all
            ? window.event.clientY
            : e.pageY;

        if ( this.selected !== null ) {
            var annotatorRevertScale = this.parent.getAttribute('data-revert-scale');
            var annotatorZoomLevel = this.parent.getAttribute('data-zoom-level');
            var left = this.x_pos - this.x_elem;
            var top = this.y_pos - this.y_elem;

            this.el.style.left = left + 'px';
            this.el.style.top = top + 'px';
            const newR = {
                x: left * annotatorRevertScale / annotatorZoomLevel,
                y: top * annotatorRevertScale / annotatorZoomLevel,
                width:this.currentRect.width,
                height:this.currentRect.height
            };
            this.setSelectionRect( newR, true )
            //console.log( 'stored moving', newR )

        }
    }

    _drag_init = ( e ) => {
        // Store the object of the element which needs to be moved
        if ( this.initialDrawComplete ) {
            e.stopPropagation( );
            this.selected = e;
            this.x_elem = this.x_pos - this.el.offsetLeft;
            this.y_elem = this.y_pos - this.el.offsetTop;
            // var event = new CustomEvent('selection', { detail: { rect: rect, selectionComplete: true } });
            // this.el.dispatchEvent(event);
            //console.log( 'stored', rect )
            return false;
        }
    }

    _initResize = (e) => {
        window.addEventListener('mousemove', this._resize, false);
        window.addEventListener('mouseup', this._stopResize, false);

        this.el.removeEventListener( "mousedown", this._drag_init, false );
        this.el.removeEventListener( "mousemove", this._move_elem, false );
        this.el.removeEventListener( "mouseup", this._destroy, false);
    }

    _resize = (e) => {
        var annotatorConvertScale = this.parent.getAttribute('data-convert-scale');
        var annotatorRevertScale = this.parent.getAttribute('data-revert-scale');
        var annotatorZoomLevel = this.parent.getAttribute('data-zoom-level');
        var width = (parseInt(this.currentRect.width*annotatorConvertScale*annotatorZoomLevel, 10) + e.movementX)
        var height = (parseInt(this.currentRect.height*annotatorConvertScale*annotatorZoomLevel, 10) + e.movementY)

        this.currentRect.width = width*annotatorRevertScale/annotatorZoomLevel;
        this.currentRect.height = height*annotatorRevertScale/annotatorZoomLevel;

        this.el.style.width = width +'px';
        this.el.style.height = height + 'px';
    }

    _stopResize = (e) => {
        window.removeEventListener('mousemove', this._resize, false);
        window.removeEventListener('mouseup', this._stopResize, false);

        this.el.addEventListener( "mousedown", this._drag_init, false );
        this.el.addEventListener( "mousemove", this._move_elem, false );
        this.el.addEventListener( "mouseup", this._destroy, false);
    }
}

export default Annotation;
