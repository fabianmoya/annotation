//@flow
import Annotation from './Annotation';
import SelectionHandler from './SelectionHandler';
import type {Size, ScrollPercentage} from './types';

class Annotator {
  c: HTMLElement; // container element
  cs: Size; // container size
  el: HTMLDivElement; // primary annotator element;
  url: string; // source image url
  imageId: number = 0; // Annotations will be assigned to this Image Id
  is: Size; // image original size
  ds: Size; // display size, what size to display the main element
  sc: HTMLDivElement; // scroll container
  scds: Size; // scroll container display size
  convertScale: number; // the display scale of the image, from this number we can calculate the position against the full sized image
  revertScale: number; // scale to revert numbers back to the size of the original image which is all that is stored in the Annotation object
  m: number = 1; // multiplier, this is used to set zoom level (multiplied against the scale).  Values are: 1,2,4,6,8.  Zooming just doubles the number until we hit our max(8) or min(1)
  occluded: boolean = false;
  resizeMode: boolean = false; // enables to resize the current annotations dimmensions
  newAnnotationAdded: boolean = false;
  historicDraws: number = 0; // limits the number of annotations that can be drawed
  maxDraws: number = 1; // limits the number of annotations that can be drawed
  sp: ScrollPercentage = { top: 0, left: 0 }; // scroll percentage, stores the value of the scroll offset as a percentage of the scrollable area

  annotations: [];
  sh: SelectionHandler;
  activeAnno: Annotation;

  constructor(element: HTMLElement) {
    this.c = element;
    this.setupElements();
    // setTimeout(( ) => {
    //   this.loadAnnoation({ id: "123456", top:56, left:92, width: 192, height: 140 });
    // }, 1000);
  }

  setupElements() {
    // ensure correct css on container
    this.c.style.display = "flex";
    this.c.style.alignItems = "center";
    this.c.style.justifyContent = "center";
    //$FlowFixMe
    this.c.style.userSelect = "none";

    // create and set scroll container element
    this.sc = document.createElement("div");
    this.sc.id = "scroll-container";
    this.sc.addEventListener("scroll", this.handleScrollEvent, false);

    this.c.appendChild(this.sc);

    // create and set scroll content element
    this.el = document.createElement("div");
    this.el.id = "annotator";
    this.el.style.position = "relative";
    this.el.style.backgroundSize = "contain";
    this.el.style.backgroundRepeat = "no-repeat";
    this.el.style.cursor = "pointer";
    this.sc.appendChild(this.el);

    // attach SelectionHandler

    this.sh = new SelectionHandler(this.el);
    this.sh.addEventListener("selection", this.handleSelection, false);

    // Annotation for hadnling a new selection
    this.annotations = [];

    //update zoom level and scale info
    this.el.dataset.zoomLevel = this.m;
    //document.getElementById("numberDraws").value = this.numberDraws;
  }

  updateDisplay(annotationList = null, color = 'white') {
    // gather calculations
    this.cs = this.elementSize(this.c);
    this.convertScale = this.calcScale(this.cs, this.is);
    this.revertScale = this.calcScale(this.cs, this.is, true)
    this.scds = this.calcDisplaySize(this.convertScale, this.cs, this.is); // scroll container display size
    this.ds = this.calcDisplaySize(this.convertScale, this.cs, this.is, this.m); // scroll content display size

    // size the scroll container
    this.sc.style.width = this.scds.width + "px";
    this.sc.style.height = this.scds.height + "px";
    this.sc.style.overflow = "hidden";
    this.sc.style.overflow = this.m === 1?"hidden":"auto";

    // size the scroll content and set image
    this.el.style.width = this.ds.width + "px";
    this.el.style.height = this.ds.height + "px";
    this.el.style.backgroundImage = `url(${this.url})`;

    this.setScrollOffset();

    //update zoom level and scale info
    this.el.dataset.convertScale = this.convertScale;
    this.el.dataset.revertScale = this.revertScale;

    if(annotationList != null){
      this.addAnnotations(annotationList, color);

      for(var index = 0; index < this.annotations.length; index++){
        this.historicDraws++;
        this.annotations[index].updateDisplay(this.convertScale, this.m);
      }

      this.activeAnno = undefined;
    }
  }

  load(data) {
      if(data.job_type === "annotate"){
        this.url = data.image_url;
      } else {
        this.url = data.img_obj.image_url;
      }
      this.getImgSize(this.url)
      .then((size) => {
        this.is = size;
        let annotationList = [];
        if(data.job_type === "verify"){
          annotationList.push(data.annotation);
          this.updateDisplay(annotationList);
          this.maxDraws = 0;

        }else if(data.job_type === "confirm"){
          data.annotation.forEach(function(annotation){
            annotationList.push(annotation.coordinates);
          });
          this.updateDisplay(annotationList);
          this.maxDraws = 0
        } else if(data.job_type === "annotate"){
          this.updateDisplay(data.previous_annotations, 'springgreen');
        }
      })
      .catch((err) => {
        console.log("Image url error");
      });
  }

  getImgSize(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let i = new Image();
      i.addEventListener("load", () => {
        const s = {width: i.width, height: i.height};
        resolve(s);
      });
      i.addEventListener("error", () => {
        reject(new Error("There was an error loading the image."))
      });
      i.src = url;
    });
  }

  calcScale(rect1: Size, rect2: Size, inverted: false): number {
    const cr = rect1.width / rect2.width;
    const ir = rect1.height / rect2.height;
    const crInv = rect2.width / rect1.width;
    const irInv = rect2.height / rect1.height;
    let scale;
    if (cr > ir) {
      scale = inverted? irInv: ir;
    }
    else {
      scale = inverted? crInv: cr;
    }

    //scale = rect1.height / rect2.height;//temp fix
    return scale;
  }

  elementSize(el: Element): Size {
    const cs = window.getComputedStyle(el);
    return {width: parseInt(cs.width, 10), height: parseInt(cs.height, 10)};
  }

  calcDisplaySize(scale: number, c: Size, i: Size, m: number = 1): Size {
    const w = Math.round(i.width * scale * m);
    const h = Math.round(i.height * scale * m);
    return {width: w, height: h};
  }

  zoomIn(): number {
    const newZoomLevel = this.m * 2;
    if(newZoomLevel <= 8) {
      this.m = newZoomLevel;
      this.el.dataset.zoomLevel = this.m;
      this.updateDisplay();
      for(var index = 0; index < this.annotations.length; index++){
        this.annotations[index].updateDisplay(this.convertScale, this.m);
      }
    }
    return this.m;
  }

  zoomOut(): number {
    const newZoomLevel = this.m / 2;
    if(newZoomLevel >= 1) {
      this.m = newZoomLevel;
      this.el.dataset.zoomLevel = this.m;
      this.updateDisplay();
      for(var index = 0; index < this.annotations.length; index++){
        this.annotations[index].updateDisplay(this.convertScale, this.m);
      }
    }
    return this.m;
  }

  toggleResizeMode(): boolean{
    this.resizeMode = !this.resizeMode;
    for(var index = 0; index < this.annotations.length; index++){
      if (this.resizeMode){
        this.annotations[index].resizer.style.display = 'block';
      } else {
        this.annotations[index].resizer.style.display = 'none';
      }
    }
    return this.resizeMode;
  }

  toggleOccluded(): boolean{
    this.occluded = !this.occluded;

    return this.occluded;
  }

  setScrollOffset() {
    const availH = this.ds.height - this.scds.height;
    const availW = this.ds.width - this.scds.width;
    const st = availH * this.sp.top;
    const sl = availW * this.sp.left;
    this.sc.scrollTop = st;
    this.sc.scrollLeft = sl;
  }

  threeInsertions(){
    for(var index = 0; index < 3; index++){
      const newR = {
        x: index * 10,
        y: index * 10,
        width: 100,
        height: 100
      };
      //
      if(this.activeAnno === undefined){
        this.activeAnno = new Annotation(this.el);
      }
      this.activeAnno.setSelectionRect(newR, true);
      this.annotations.push(this.activeAnno);
      this.activeAnno.updateDisplay(this.convertScale, this.m);
      this.activeAnno = undefined;
    }
  }

  addAnnotations(annotations, color){
    annotations.forEach((annotation) => {

      const newR = {
        x: annotation.x,
        y: annotation.y,
        width: annotation.width,
        height: annotation.height
      };

      this.activeAnno = new Annotation(this.el, color);
      this.activeAnno.editable = annotation.editable ? true : false;

      if(!annotation.editable) {
        this.activeAnno.removeActions();
      }

      this.activeAnno.setSelectionRect(newR, true);
      this.annotations.push(this.activeAnno);
      this.activeAnno.updateDisplay(1, 1);
    });
  }

  clearAnnotations(){
    var last;
    // while (last = this.el.lastChild){
    console.log('this.newAnnotationAdded',this.newAnnotationAdded)
    if (this.newAnnotationAdded === true){
         this.el.removeChild(this.el.lastChild);
    }

    // }

    this.historicDraws = 0;
    this.newAnnotationAdded = false;
    this.annotations = [];
  }

  getLastAnnotationData(){
    if(this.annotations.length === 0){
      return false;
    } else{
      return this.annotations[this.annotations.length-1].currentRect;
    }
  }

  handleScrollEvent = (e: any) => {
    const t = e.target;
    let topP = t.scrollTop / (this.ds.height - this.scds.height);
    topP = topP < 0 ? 0 : topP > 1 ? 1 : topP;
    let leftP = t.scrollLeft / (this.ds.width - this.scds.width);
    leftP = leftP < 0 ? 0 : leftP > 1 ? 1 : leftP;
    this.sp = {top: topP, left: leftP};
  }

  handleSelection = (event: any) => {
    const rect = event.detail.rect;
    const selComplete = event.detail.selectionComplete;
    //Validates the number of draws
    if(rect && this.annotations.length < (this.historicDraws + this.maxDraws) && !this.resizeMode) {
      const newR = {
        x: (rect.x * this.revertScale) / this.m,
        y: (rect.y * this.revertScale) / this.m,
        width: (rect.w * this.revertScale) / this.m,
        height: (rect.h * this.revertScale) / this.m
      };
      //
      if(this.activeAnno === undefined){
        this.activeAnno = new Annotation(this.el, 'white');
        this.activeAnno.editable = this.editable;
      }
      this.activeAnno.setSelectionRect(newR, selComplete);
      this.activeAnno.updateDisplay(this.convertScale, this.m, 'white');

      if(selComplete){
        this.annotations.push(this.activeAnno);
        this.activeAnno.updateDisplay(this.convertScale, this.m, 'white');
        this.activeAnno = undefined;
        if(this.annotations.length >= (this.historicDraws + this.maxDraws)) {
          this.newAnnotationAdded = true;
        }
      }
    }
  }
}

export default Annotator;
