//@flow
import React from 'react';

import '../../styles-base/index.scss';
import './styles/index.scss';

import Annotator from './lib/Annotator';
import Spinner from '../../common/Spinner';
import service from '../../services/AnnotationService';

import thumbs_down from '../../common/assets/thumbs_down.svg';
import thumbs_up from '../../common/assets/thumbs_up.svg';

import $ from 'jquery';
// import 'jquery-ui';
import './lib/selectareas';
    $(document).ready(function () {
        $('#btnView').click(function () {
            var areas = $('#annotator').selectAreas('areas');
            displayAreas(areas);console.log('destroyed2');
        });
    });

    function areaToString (area) {
        return (typeof area.id === "undefined" ? "" : (area.id + ": ")) + area.x + ':' + area.y  + ' ' + area.width + 'x' + area.height + '<br />'
    }

    function output (text) {
        $('#output').html(text);
    }

    /* Log the quantity of selections
    function debugQtyAreas (event, id, areas) {
        console.log(areas.length + " areas", arguments);
    };*/

    // Display areas coordinates in a div
    function displayAreas (areas) {
        var text = "";
        $.each(areas, function (id, area) {
            text += areaToString(area);
        });
        output(text);
    };

type Props = {};

type State = {
    zoomLevel: number
};

class AnnotateJob extends React.Component < Props,
State > {
    state = {
        jobType: null,
        zoomLevel: 1,
        isOccluded: false,
        isResizeMode: false,
        modal: 'none',
        annotationData: false,
        isFetching: true,
        errorList: null,
        isAnnotateAdded: false,
        label:''
    };

    annotator : Annotator;

    componentDidMount( ) {
        const e = document.querySelector( ".test" );
        e.innerHtml = "";
        if ( e ) {
            service.getRandomTask().then((data) => {
                this.annotator.load(data);
                this.setState({jobType: data.job_type});
                this.setState({errorList: data.error_obj});
                this.setState({label: data.img_obj.image_label});

            }).catch((err) => {
                console.log("Image service error");
                this.setState({errorList: 0})
            });
            this.annotator = new Annotator( e );
            setTimeout(function() { this.setState({isFetching: false}); }.bind(this), 1000);
            if (this.props.annotations) {
                this.annotator.addAnnotations(this.props.annotations);
            }
        }
    }

    viewModal = () => {
        const annotationData = this.annotator.getLastAnnotationData();
        if(annotationData !== false){
            annotationData['image_id'] = this.annotator.imageId;
            annotationData['occluded'] = this.state.isOccluded;
            annotationData['nothing_to_add'] = false;
        }
        if(this.annotator.historicDraws === this.annotator.annotations.length){
            this.setState({isAnnotateAdded: this.annotator.newAnnotationAdded});
        }
        this.setState({modal: 'flex', annotationData: annotationData});
    }
    closeModal = () => {

        this.setState({modal: 'none'});
    }

    handleZoomIn = ( ) => {
        const l = this.annotator.zoomIn();
        this.setState({ zoomLevel: l });
    };

    handleZoomOut = ( ) => {
        const l = this.annotator.zoomOut();
        this.setState({ zoomLevel: l });
    };

    handleNumberDrawsChange = (event) => {
        this.setState({maxNumberDraws: event.target.value});
        this.annotator.numberDraws = event.target.value;
    };

    handleClearAnnotations = (event) => {
        event.preventDefault();
        this.annotator.clearAnnotations();
        this.setState({isAnnotateAdded: false});
    };

    handleThreeInsertions = () => {
        this.annotator.threeInsertions();
    };

    handleResizeMode = (event) => {
        this.setState({
          isResizeMode: this.annotator.toggleResizeMode()
        });
    }

    handleEditableAnno = (event) => {
        this.annotator.editable = event.target.checked;
    };

    handleShowEditableAnno = (event) => {
        if(this.annotator.annotations.length > 0) {
            this.annotator.annotations.forEach((annotation) => {
                if(event.target.checked) {
                    annotation.el.style.display = annotation.editable ? 'block' : 'none';
                } else {
                    annotation.el.style.display = 'block';
                }
            })
        }
    };

    handleOccludedState = (event) => {
        this.setState({
          isOccluded: this.annotator.toggleOccluded()
        });
    };

    handleTextNameAnnotation = (event) => {
        if( event.target.value !== '') {
            var annot = this.state.annotationData;
            annot['name'] = event.target.value;

            this.setState({annotationData: annot});
        }
    }

    handleSaveAnnotation = (event) => {
        var annot = this.state.annotationData;
       console.log(annot);
       this.closeModal();
       this.setState({isFetching: true});
       setTimeout(function() { window.location.reload(true);}, 2000);
    };

    handleReview = (event) => {
        if(this.state.jobType === "verify") {
           this.setState({isFetching: true});
           setTimeout(function() { window.location.reload(true);}, 2000);
        }else if(this.state.jobType === "confirm") {
           this.setState({isFetching: true});
           setTimeout(function() { window.location.reload(true);}, 2000);
        }
    };

    render( ) {
        const Checkbox = (props) => {
            return (
                <div>
                    <input type="checkbox" onClick={props.clickEvent} />
                    <label>{props.label}</label>
                </div>
            )
        };

        let AnnotationInfoModal = null;
        if(this.state.jobType !== "annotate"){
            AnnotationInfoModal = (
                <div>
                    <div className="annotation-info">
                        <ul>
                        {this.state.errorList?
                            this.state.errorList.map((item, index) => {
                                return (

                                    <li key={item.error_id}>

                                        <input id={`checkbox-${index}`} className="checkbox-custom" name={`checkbox-${index}`} type="checkbox"/>
                                        <label htmlFor={`checkbox-${index}`} className="checkbox-custom-label">{JSON.stringify(item.error_name)}</label>
                                    </li>
                                );
                            })
                            :null}
                        </ul>
                    </div>

                    <div className="annotation-save-form">
                        <div className="popup-ctas">
                            <button className="saveBtn" onClick={this.handleSaveAnnotation}>Save</button>
                        </div>
                    </div>
                </div>
            )
        }else if(this.annotator.newAnnotationAdded){
            AnnotationInfoModal = (
                <div>
                    <div className="annotation-info">
                        <h3>Annotation Info: {this.state.annotationData['name']}</h3>
                        <ul>
                            <li>ImageId: {this.state.annotationData['image_id']}</li>
                            <li>X Position: {this.state.annotationData['x']}</li>
                            <li>Y Position: {this.state.annotationData['y']}</li>
                            <li>Width: {this.state.annotationData['width']}</li>
                            <li>Height: {this.state.annotationData['height']}</li>
                            <li>Occluded: {JSON.stringify(this.state.annotationData['occluded'])}</li>
                            {/*<li>nothing_to_add: {JSON.stringify(this.state.annotationData['nothing_to_add'])}</li>*/}
                        </ul>
                    </div>

                    <div className="annotation-save-form">
                        <div className="popup-ctas">
                            <button className="saveBtn" onClick={this.handleSaveAnnotation}>Save</button>
                        </div>
                    </div>
                </div>
            );
        } else {
            AnnotationInfoModal = (
                <div className="annotation-info">
                    <h3> You should create an annotation to save any data</h3>
                </div>
            );
        }
        const modalContent = (
            <div className="anno-job__popup" style={{display: this.state.modal}}>
                <div className="popup-wrapper">
                    <div className="content">
                        <span className="closeModal" onClick={this.closeModal}>X</span>
                        {AnnotationInfoModal}

                    </div>
                </div>
            </div>
        )

        return (
            <div>
                <div className="tags">
                    <h1 className="mainHeader">
                        {this.state.jobType === "annotate"? `If possible, tag any more tags`:
                            this.state.jobType === "verify"? `Is the [${this.state.label}] tagged correctly? - Verify`:
                                this.state.jobType === "confirm"? `Are all [${this.state.label}] items tagged? - Confirm`:
                                    null}
                    </h1>
                </div>
                <div className="anno-job">
                    <div className="test">
                    {this.state.isFetching?<Spinner/>: null}
                    </div>
                    <div className="anno-job__image-controls">
                        {this.state.jobType === "annotate"?
                            <a className="anno-job__image-controls__reset" onClick={this.handleClearAnnotations} href="">Clear tag</a>
                            :<a className="anno-job__image-controls__reset" onClick={this.handleClearAnnotations} href="" style={{opacity:'0'}}>Clear tag</a>}
                        <div className="anno-job__image-controls__magnifier">
                            <button type="button" onClick={this.handleZoomOut}>-</button>
                            <span>{this.state.zoomLevel}x</span>
                            <button type="button" onClick={this.handleZoomIn}>+</button>
                        </div>
                    </div>
                    {this.state.jobType === "annotate"?
                        <div className="anno-job__saving-controls">
                            <button onClick={this.handleSaveAnnotation}>Nothing to Add</button>
                            <div className="occluded-checkbox">
                                <input id="checkbox-1" className="checkbox-custom" onClick={this.handleOccludedState.bind(this)} name="checkbox-1" type="checkbox"/>
                                <label htmlFor="checkbox-1" className="checkbox-custom-label">This tag is occluded</label>
                            </div>

                            <button onClick={this.viewModal}>Submit</button>
                        </div>
                        :<div className="anno-job__review-controls">
                            <img className="tagging__image" src={thumbs_down} alt="Don't agree" onClick={this.viewModal}/>
                            <img className="tagging__image" src={thumbs_up} alt="Agree" onClick={this.handleReview.bind(this)}/>
                        </div>}

                    {/*<div>
                        Max number of draws: <input type="number" id="numberDraws" value={this.state.maxNumberDraws} onChange={this.handleNumberDrawsChange} disabled/>
                    </div>
                    <button onClick={this.handleThreeInsertions} disabled>Insert 3</button>
                    <button onClick={this.handleClearAnnotations}>Reset</button>
                    <div>
                        <Checkbox clickEvent={this.handleEditableAnno.bind(this)} label="Add only editable annotations"/>
                    </div>
                    <div>
                        {this.props.annotations ?
                            <Checkbox clickEvent={this.handleShowEditableAnno.bind(this)} label="Show only editable annotations"/>
                        : null }
                    </div>*/}
                    {modalContent}
                </div>
            </div>
        );
    }
}

export default AnnotateJob;
