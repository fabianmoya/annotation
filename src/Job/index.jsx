import React from 'react';
import './styles/index.scss';
import AnnotateJob from './AnnotateJob';

class Job extends React.Component {
    render() {
        return (
            <div className="job">
                    <AnnotateJob/>
            </div>
        );
    }
}

export default Job;
