//@flow
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Spinner from '../common/Spinner';
import { loginUser } from '../actions/userActions';
import logo from '../common/assets/logo.svg';
import './styles/index.scss';

type Props = {
    isFetching: boolean,
    errorMessage:
        ? string,
    onLoginSubmit: Function,
    handleSubmit:Function,
    location: Object
};

class Login extends React.Component < Props > {
    handleSubmit = (event) =>{
        event.preventDefault();
        const username = this.refs.username.value.trim();
        const password = this.refs.password.value.trim();
        this.props.onLoginSubmit( username, password )
    }
    render( ) {
        if ( this.props.isLoggedIn ) {
            const { from: redirectPath } = this.props.location.state || {
                from: {
                    pathname: '/'
                }
            }
            return <Redirect to={redirectPath}/>;
        }

        let submitButtonArea;
        if ( this.props.isFetching ) {
            submitButtonArea = <Spinner/>
        } else {
            submitButtonArea = <button className="loginBtn">login</button>;
        }

        return (
            <div className="login">

                <form onSubmit={this.handleSubmit}>
                    <img className="logo" src={logo} alt="CYNGN LOGO"/>
                    <h2>CYNGN Annotation Application</h2>
                    <div className="divider"/> {this.props.errorMessage
                        ? <i className="login-error">* {this.props.errorMessage}</i>
                        : null}
                    <input ref="username" type="text" placeholder="Username"/>
                    <input ref="password" type="password" placeholder="Password"/>
                    <div className="bottom-row">
                        <a href="/" className="forgotPassword">Forgot Password?</a>
                        {submitButtonArea}
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({ isFetching: state.user.isFetching, isLoggedIn: state.user.isLoggedIn, errorMessage: state.user.errorMessage });

const mapDispatchToProps = ( dispatch ) => ({
    onLoginSubmit( u : string, p : string ) {
        dispatch(loginUser( u, p ));
    }
});

export default connect( mapStateToProps, mapDispatchToProps )( Login );
