import React from 'react';
// import './styles/index.scss';
import AnnotateJob from '../Job/AnnotateJob';

class EditJob extends React.Component {
    render() {
        const annotations = [
            {
                x: 20,
                y: 215,
                width: 20,
                height: 20,
                editable: true
            }, {
                x: 313,
                y: 215,
                width: 20,
                height: 20
            }, {
                x: 192,
                y: 116,
                width: 50,
                height: 50
            }, {
                x: 48,
                y: 31,
                width: 80,
                height: 80
            }
        ];
        return (
            <div className="job">
                <div className="appMainWrapper">
                    <div className="tags">
                        <h1 className="mainHeader">Edit Job</h1>

                    </div>
                    <AnnotateJob annotations={annotations}/>
                </div>
            </div>
        );
    }
}

export default EditJob;
