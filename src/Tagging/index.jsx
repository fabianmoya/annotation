//@flow
import React from 'react';
import './styles/index.scss';
import temporalImage from '../common/assets/car.png';
import thumbs_down from '../common/assets/thumbs_down.svg';
import thumbs_up from '../common/assets/thumbs_up.svg';

type Props = {};

type State = {
    modal: string
}

class tagging extends React.Component < Props,
State > {
    constructor(props : Props) {
        super(props);
        this.state = {
            modal: 'none'
        };
    }
    viewModal = () => {

        this.setState({modal: 'flex'});
    }
    closeModal = () => {

        this.setState({modal: 'none'});
    }
    render() {

        let modalContent = (
            <div className="tagging__popup" style={{
                display: this.state.modal
            }}>
                <div className="popup-wrapper">
                    <div className="content">
                        <span className="closeModal" onClick={this.closeModal}>X</span>
                        <ul className="popup-options">
                            <li>

                                <input id="checkbox-1" className="checkbox-custom" name="checkbox-1" type="checkbox"/>
                                <label htmlFor="checkbox-1" className="checkbox-custom-label">wront tag</label>

                            </li>
                            <li>

                                <input id="checkbox-2" className="checkbox-custom" name="checkbox-2" type="checkbox"/>
                                <label htmlFor="checkbox-2" className="checkbox-custom-label">tag(s) size around object is to large</label>

                            </li>
                            <li>

                                <input id="checkbox-3" className="checkbox-custom" name="checkbox-3" type="checkbox"/>
                                <label htmlFor="checkbox-3" className="checkbox-custom-label">tag(s) doesn't cover entire object</label>

                            </li>
                            <li>

                                <input id="checkbox-4" className="checkbox-custom" name="checkbox-4" type="checkbox"/>
                                <label htmlFor="checkbox-4" className="checkbox-custom-label">missing tag(s)</label>

                            </li>
                            <li>

                                <input id="checkbox-5" className="checkbox-custom" name="checkbox-5" type="checkbox"/>
                                <label htmlFor="checkbox-5" className="checkbox-custom-label">incorrect occlusion</label>

                            </li>

                            <li>

                                <input id="checkbox-6" className="checkbox-custom" name="checkbox-6" type="checkbox"/>
                                <label htmlFor="checkbox-6" className="checkbox-custom-label">other</label>

                            </li>

                        </ul>
                        <div className="popup-ctas">
                            <button className="saveBtn">submit</button>
                        </div>
                    </div>

                </div>

            </div>
        )

        return (
            <div>
                <div className="tagging">
                    <h1 className="mainHeader">Are all the [TAG_NAME] items tagged?</h1>
                    <div className="wrapper">
                        <ul className="tagging__guideline">
                            <li>is is where guidline number one will be displayed</li>
                            <li>is is where guidline number one will be displayed</li>
                            <li>THIS TAG HAS BEEN MARKED AS OCCLUDED</li>
                            <li className="openModal" onClick={this.viewModal}>temporary popUp</li>
                        </ul>
                        <div className="tagging__content">
                            <img className="tagging__image" src={temporalImage} alt="Tag" id="sky"/>
                        </div>
                        <div className="tagging__magnifier">
                            <button type="button">-</button>
                            <span>1x</span>
                            <button type="button">+</button>

                        </div>
                        <div className="tagging__ctas">
                            <img className="tagging__image" src={thumbs_down} alt="Don't agree"/>
                            <img className="tagging__image" src={thumbs_up} alt="Agree"/>
                        </div>
                        {modalContent}
                    </div>

                </div>

            </div>
        );
    }
}

export default tagging;
