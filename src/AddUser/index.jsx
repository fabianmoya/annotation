//@flow
import React from 'react';
// import { connect } from 'react-redux';
// import { Redirect } from 'react-router-dom';
// import { loginUser } from '../actions/userActions';

import './styles/index.scss';

type Props = {

};

class addUser extends React.Component < Props > {

    render( ) {

        return (
            <div className="addUser">
                <h1 className="mainHeader">Add New User</h1>
                <form>
                    <h3>Add the email of the user you would like to invite</h3>
                    <input ref="email" type="email" placeholder="e-mail"/>


                     <button className="sentBtn">send</button>

                </form>
            </div>
        );
    }
}



export default addUser;
