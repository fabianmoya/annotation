// @flow

import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
  CHECK_FOR_LOGIN_SUCCESS
} from '../actions/userActions';

type State = {
  isFetching: boolean,
  isLoggedIn: boolean,
  errorMessage: ?string
};

const userReducer = (state: State = {
  isFetching: false,
  isLoggedIn: false,
  errorMessage: null
}, action: Action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, { isFetching: true, isLoggedIn: false, errorMessage: null });
    case LOGIN_SUCCESS:
      return Object.assign({}, state, { isFetching: false, isLoggedIn: true, errorMessage: null });
    case LOGIN_FAILURE:
      return Object.assign({}, state, { isFetching: false, isLoggedIn: false, errorMessage: action.payload });
    case LOGOUT_SUCCESS:
      return Object.assign({}, state, { isFetching: false, isLoggedIn: false });
    case CHECK_FOR_LOGIN_SUCCESS:
      return Object.assign({}, state, { isLoggedIn: true });
    default:
      return state;
  }
};

export default userReducer;
