// @flow

import auth from '../services/AuthService';

/* Action Constants */
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const CHECK_FOR_LOGIN_SUCCESS = 'CHECK_FOR_LOGIN_SUCCESS';

/* Action Creators */
const requestLogin = () => ({ type: LOGIN_REQUEST, payload: null });
const receiveLogin = (user) => ({ type: LOGIN_SUCCESS, payload: null });
const receiveLoginError = (message) => ({ type: LOGIN_FAILURE, payload: message });
const receiveLogout = () => ({ type: LOGOUT_SUCCESS, payload: {} });
export const successfulCheckForLogin = () => ({ type: CHECK_FOR_LOGIN_SUCCESS });

/* Async Actions */
export function loginUser (username: string, pass: string) {
  return (dispatch: Function) => {
    dispatch(requestLogin());
    if(!username || !pass){
      dispatch(receiveLoginError("Username and password required."))
    }
    else{
      auth
        .login(username, pass)
        .then(() => {
          dispatch(receiveLogin());
        })
        .catch(err => {
          //console.log(err);
          dispatch(receiveLoginError(err))
        });
    }
  }
}

export function logoutUser () {
  return (dispatch: Function) => {
    auth.logout();
    dispatch(receiveLogout());
  }
}
