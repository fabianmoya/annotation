import React from 'react';
import styled, {keyframes} from 'styled-components';
import imgWhite from './spinner-white.png';
import img from './spinner.svg';

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100%{ transform: rotate(360deg); }
`;

const Image = styled.img`
  animation: ${spin} 2s infinite linear;
`;

const Spinner = (props) => (
  <Image className="spinner" src={props.color === "white" ? imgWhite : img} alt="loading indicator" />
);

export default Spinner;
