//@flow
import React from 'react';
// import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Spinner from '../common/Spinner';
// import { loginUser } from '../actions/userActions';
import logo from '../common/assets/logo.svg';
import check from '../common/assets/check_icon.svg';
import './styles/index.scss';

type Props = {
    errorMessage:string,
    onLoginSubmit:Function,
location:Object,
};

type State ={

}

class Registration extends React.Component < Props, State > {
    handleSubmit = ( ) => {}
    render( ) {
        if ( this.props.isLoggedIn ) {
            const { from: redirectPath } = this.props.location.state || {
                from: {
                    pathname: '/'
                }
            }
            return <Redirect to={redirectPath}/>;
        }

        let submitButtonArea;
        if ( this.props.isFetching ) {
            submitButtonArea = <Spinner/>
        } else {
            submitButtonArea = <button className="loginBtn" onClick={( ) => {
                this.props.onLoginSubmit( "John", "password" )
            }}>register</button>;
        }

        return (
            <div>
                <div className="registration">
                    <img className="logo" src={logo} alt="CYNGN LOGO"/>
                    <h2>CYNGN Annotation Application</h2>
                    <h3>New User Registration</h3>
                    <form onSubmit={this.handleSubmit}>

                        <div className="divider"/> {this.props.errorMessage
                            ? <i className="login-error">* {this.props.errorMessage}</i>
                            : null}
                        <input ref="name" type="text" placeholder="name"/>
                        <input ref="email" type="email" placeholder="e-mail"/>
                        <input ref="phone" type="text" placeholder="phone number"/>
                        <input ref="password" type="password" placeholder="password"/>
                        <input ref="password2" type="password" placeholder="retype password"/>
                        <div className="bottom-row">

                            {submitButtonArea}
                        </div>
                    </form>
                </div>
                <div className="registration">
                    <img className="logo" src={logo} alt="CYNGN LOGO"/>
                    <h2>CYNGN Annotation Application</h2>
                    <h3>New User Registration</h3>
                    <div className="registration__success">
                        <img className="check" src={check} alt="CYNGN LOGO"/>
                        <span>Registration Successful</span>
                    </div>
                    <div className="bottom-row">
                        <button className="loginBtn">enter</button>;
                    </div>

                </div>
            </div>

        );
    }
}

// const mapStateToProps = state => ({ isFetching: state.user.isFetching, isLoggedIn: state.user.isLoggedIn, errorMessage: state.user.errorMessage });

// const mapDispatchToProps = ( dispatch ) => ({
//     // onLoginSubmit( u : string, p : string ) {
//     //     dispatch(loginUser( u, p ));
//     // }
// });

export default Registration;
