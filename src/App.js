//@flow
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';
import { successfulCheckForLogin } from './actions/userActions';
import Header from './Header';
import Login from './Login';
import Job from './Job';
import EditJob from './editJob';
import addUser from './AddUser';
import tags from './Tags';
import tagging from './Tagging';
import Registration from './Registration';
import auth from './services/AuthService';
import PrivateRoute from './common/PrivateRoute';

const FourOhFour = () => <h1>404</h1>;

type Props = {
    isLoggedIn: boolean,
    handleRehydrate: Function
};

class App extends React.Component<Props> {
    componentWillMount() {
        if(auth.isLoggedIn()){
            this.props.handleRehydrate();
        }
    }

  chooseRandomly = () => {
    const pages = [ Job, addUser, tagging, Registration ];
    return pages[Math.floor((Math.random() * pages.length))];
  }

  render() {
    const loc = window.location;
    return (
      <BrowserRouter>
        <div id="App">
          <Header />
          <div className="routes">
            <div className="appMainWrapper">
              <Switch>
                <PrivateRoute exact path="/" isLoggedIn={this.props.isLoggedIn} location={loc} component={Job} />
                <Route exact path="/" component={Login} />
                <Route exact path="/job" isLoggedIn={this.props.isLoggedIn} location={loc} component={Job} />
                <Route exact path="/editjob" isLoggedIn={this.props.isLoggedIn} location={loc} component={EditJob} />
                <Route exact path="/tagging" isLoggedIn={this.props.isLoggedIn} location={loc} component={tagging} />
                <Route exact path="/addUser" isLoggedIn={this.props.isLoggedIn} location={loc} component={addUser} />
                <Route exact path="/tags" isLoggedIn={this.props.isLoggedIn} location={loc} component={tags} />
                <Route exact path="/registration" isLoggedIn={this.props.isLoggedIn} location={loc} component={Registration} />
                <Route exact path="/login" component={Login} />
                <Route component={FourOhFour} />
              </Switch>
            </div>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
    isLoggedIn: state.user.isLoggedIn
});

const mapDispatchToProps = (dispatch) => ({
    handleRehydrate() {
        dispatch(successfulCheckForLogin());
    }
});

export const Unwrapped = App;
export default connect(mapStateToProps, mapDispatchToProps)(App);
