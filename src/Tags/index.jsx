//@flow
import React from 'react';
import './styles/index.scss';

type Props = {

};

class tags extends React.Component < Props > {

    render( ) {

        return (
            <div className="tags">
                <h1 className="mainHeader">Tags</h1>
                <div className="wrapper">

                    <ul className="tags__list">
                        <li className="tags__item">Cars
                            <span>remove</span>
                        </li>
                        <li className="tags__item">People
                            <span>remove</span>
                        </li>
                        <li className="tags__item">Trees
                            <span>remove</span>
                        </li>
                        <li className="tags__item">Street Signs
                            <span>remove</span>
                        </li>
                    </ul>
                    <form>

                        <input ref="email" type="email" placeholder="add new tag"/>

                        <button className="saveBtn">save</button>

                    </form>
                </div>
            </div>
        );
    }
}

export default tags;
