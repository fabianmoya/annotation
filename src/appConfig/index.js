const USE_LOCAL_API = true;

const ConfigGenerator = () => {
  let config = { // default config, set general development settings here
    API_ROOT_URL: 'http://34.236.150.97:5000',
    ASYNC_TIMEOUT_DURATION: 10000
  };

  if(process.env.NODE_ENV === 'production') { // for production builds (which is basically any 'built' React project which can be run in staging or production) enforce these settings no matter what
    config.API_ROOT_URL = 'http://34.236.150.97:5000';
    config.ASYNC_TIMEOUT_DURATION = 20000;
  }
  else if(USE_LOCAL_API) { // if USE_LOCAL_API, use these settings.  Will not work if this project has been run through the 'build' process since 'production' is hardcoded
    config.API_ROOT_URL = 'http://localhost:3001';
  }

  return config;
};

export default ConfigGenerator();
