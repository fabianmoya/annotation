//@flow
import axios from 'axios';
import config from '../appConfig';

let g;
try {
  g = window;
}
catch(err) {
  g = global;
}

const AuthService = () => {

  function fetchLogin(username, password): Promise<any> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${config.API_ROOT_URL}/api/login`, {
          "username": username,
          "password": password
        }, {
          timeout: config.ASYNC_TIMEOUT_DURATION
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  function saveInfo(data): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        g.localStorage.setItem('token', data.token);
        g.localStorage.setItem('expiration', data.expiration);
        resolve(data);
      }
      catch (err) {
        reject(err);
      }
    });
  }

  function getToken() {
    if (g.localStorage) {
      const t = g.localStorage.getItem('token');
      return t;
    }
    return null;
  }

  function isLoggedIn() {
    const token = getToken();
    const exp = g.localStorage.getItem('expiration');
    let isAuthenticated = false;
    if(token && exp){
      const now = Date.now();
      const expiration = new Date(parseInt(exp, 10))*1000;
      if (now < expiration) {
        isAuthenticated = true;
      }
    }
    return isAuthenticated;
  }

  function logout() {
    try {
      g.localStorage.removeItem('token');
      g.localStorage.removeItem('expiration');
      return true;
    } catch (err) {
      console.error("There was an error logging out.");
      return false;
    }
  }

  function login(username: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      fetchLogin(username, password)
        .then(data => saveInfo(data))
        .then(data => resolve(data))
        .catch((err: Error) => {
          reject(err.message)
        });
    });
  }

  return {
    login: login,
    logout: logout,
    isLoggedIn: isLoggedIn
  };
};

export default AuthService();
