//@flow
import axios from 'axios';
import config from '../appConfig';

//let g;
try {
  //g = window;
}
catch(err) {
  //g = global;
}

const AnnotationService = () => {

  function getRandomTask(): Promise<any> {
    return new Promise((resolve, reject) => {
      axios
        .get(`${config.API_ROOT_URL}/api/task`)
        .then(response => {
          resolve(response.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  function saveAnnotation(annotation): Promise<any> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${config.API_ROOT_URL}/api/jobs`, annotation)
        .then(response => {
          resolve(response.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  return {
    getRandomTask: getRandomTask,
    saveAnnotation: saveAnnotation
  };
};

export default AnnotationService();
